var _ = require('lodash'),
    Promise = require('bluebird'),
    debug = require('debug')('plugin.language'),
    LanguageDetect = require('languagedetect');

var detector = new LanguageDetect();


Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.plugins = undefined;


var textLangGuess = function(unit) {
  // turn the unit into a string
  var text = JSON.stringify(unit);

  // match for anything between :" and "
  var re = /:\"(.*?)(?=\")/g;

  // run the regex
  var matches = text.match(re);

  // filter for strings that contain spaces, as these are most likely real text
  // join the array into a string
  var langtext = _.filter(matches, function (s) { return s.indexOf(' ') >= 0 }).join(' ');

  // detect the language of the string
  var language = _.first(_.first(detector.detect(langtext)));

  // merge and return the unit
  return _.merge(unit, {_lf_language: language});
};


var plugin = function(val, other) {
  return Promise.map(val.data, textLangGuess)
                .then( function(data)  {
                  var str = "Analyzed language in data elements";
                  other.log.info(str);
                  var d = _.set(val, 'data', data);
                  return d;
                });
};

plugin.description = 'Detect languages in unit';

var plugins = {
  'detect_languages': plugin
}

exports.plugins = plugins;
exports.default = { plugins: plugins };
